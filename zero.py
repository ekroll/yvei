def linear(lin_fx, margin, rounding, step_time=0.0, graph=False):
    def get_value(lin_fx, negative):
        x = 0                  # Neutral starting point

        if negative is True:   # We want a negative result
            n = 1              # Start with positive number (the while loop)
            while n > 0:
                n = lin_fx(x)  # Compute
                if n > 0:
                    x -= 1     # Decrease x if result isn't negative
                if n < 0:
                    break      # We got our answer

        else:                  # We want a positive result
            n = -1             # Start with negative number (the while loop)
            while n < 0:
                n = lin_fx(x)  # Compute
                if n < 0:
                    x += 1     # Increase x if result isn't negative
                if n > 0:
                    break      # We got our answer

        # We will get a result if the function is linear
        polarity = "negative" if negative else "positive"
        print(f"Found {polarity} number: f(x={x})={n}")

        return x

    print("Finding negative and positive borders")
    a = get_value(lin_fx, negative=True)
    b = get_value(lin_fx, negative=False)

    print(f"Aproaching f(x)=0 between f(x={a}) and f(x={b})")
    est = margin + 1  # Avoid quitting while loop

    while abs(est) > margin:
        time.sleep(step_time)

        # Get average and re-estimate
        m = (a + b) / 2
        est = round(lin_fx(m), rounding)

        # Narrow the range as we go
        a = m if est < 0 else a
        b = m if est > 0 else b

        # Programmer therapy
        print(f"\tf(x={m})={est} margin={margin} {rounding} decimal rounding")

    if graph:
        # Plotting useful range with zeropoint centered
        plotdata = np.array(range(int(m) - 10, int(m) + 10))
        plt.plot(plotdata, lin_fx(plotdata))
        plt.scatter(m, lin_fx(m))
        plt.show()

    return m


def intersects_zero(f):
    return True


def second_order(f):
    ...


def _main(args):
    def lin(x): return 1.4*x - 4.9  # A linear function
    def so(x): return x*x            # A second-order function

    lin_z = linear(                     # A zeropoint aproximation (linear)
        lin, args.margin, args.rounding,
        step_time=args.step_time,
        graph=args.graph
    )

    if intersects_zero(so):
        so_z = second_order(so)

    # Informing the user of our results
    print(f"Exp should return 0 (by margin {args.margin}) when x={lin_z}")


if __name__ == "__main__":
    import time
    import numpy as np
    import matplotlib.pyplot as plt
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--graph", default=False, action="store_true")
    args = parser.parse_args()
    _main(args)
