# Plan for Abstract Syntax Tree for Algebra
1. Get some expression in the form of text
1. Declare array
1. Declare text element
1. Loop through chars in text
1. Add chars to text element
1. Append element to array if we encounter +/-
1. Add the + or - as a separate element to the array, then skip to next iteration
1. If we find ( recurse from character index to matching ) by counting ( and )
1. If there are more ( than ) we are inside a scope
1. If there are equally many ( and ) we are outside scope (relative to recursion)
1. If there are more ) than ( your user gave you bad data
1. Return array and character index + 1 to previous recursion level

Voila, abstract syntax tree for algebra
