def main(args):
    # The two functions
    def f1(x): return 0.01*x*x+0.1*x+0.5
    def f2(x): return -0.02*x*x+0.35*x

    basedata = numpy.linspace(
        start=args.minimum,
        stop=args.maximum,
        num=args.resolution
    )

    plt.plot(basedata, f1(basedata))
    plt.plot(basedata, f2(basedata))
    #plt.scatter(m, lin_fx(m))
    plt.show()

if __name__ == "__main__":
    import matplotlib.pyplot as plt
    import argparse
    import numpy

    parser = argparse.ArgumentParser()
    parser.add_argument("--minimum",    type=int, default=0)
    parser.add_argument("--maximum",    type=int, default=1)
    parser.add_argument("--resolution", type=int, default=100)
    args = parser.parse_args()

    main(args)
