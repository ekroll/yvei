def inside_scope(open, closed):
    if open > closed:
        return True

    elif open == closed:
        return False

    else:
        print('Invalid expression: too many ")"')
        sys.exit(-1)


def parts(text):  # Splits at +/- (but preserves operator)
    return re.split('([+,-])', text)


def parse(text, i=0):   # Create abstract syntax tree
    ast = []            # Final data structure
    subexp = []         # If we encounter ()
    element = ""        # Current element being processed

    lpc = 0             # Open  ( count
    rpc = 0             # Close ) count
    e = len(text) - 1   # Last char index

    # Process specified data (used when recursing)
    text = text[i:e].replace(" ", "")

    parsing = True
    while parsing:
        if i >= e:      # Stop at end of text
            break

        # Sample character
        c = text[i]

        # Break up the expression
        if re.match("[+,-]", c):
            if inside_scope(lpc, rpc):
                subexp.append(element)
                subexp.append(c)
            else:
                ast.append(element)
                ast.append(c)
            element = ""

        # Handle sub expression
        if c == "(":
            lpc += 1

            if inside_scope(lpc, rpc):
                (subexp, i) = parse(text, i)
            else:
                ast.append(element)
                ast.append("*")

            element = ""
            continue

        # Append substuff to elements
        if c == ")":
            rpc += 1

            ast.append(subexp)
            subexp = []

            element = ""
            continue

        # Add char to element and increment index
        element += c
        i += 1

    return (ast, i)


def _main(ex):
    #ex = "2x + 3 - 15y"
    ex = "2(3/8 + 3(1/4))"

    ast = parse(ex)
    #ast = parts(ex)

    print(ex)
    print(ast)


if __name__ == "__main__":
    import sys
    import re
    _main(sys.argv[0])
